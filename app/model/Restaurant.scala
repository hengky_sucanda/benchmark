package model

/**
  * Created by hengkysucanda on 12/4/15.
  */
case class Restaurant(id: Long, name: String, address: String, email: String, phone_number: Long)