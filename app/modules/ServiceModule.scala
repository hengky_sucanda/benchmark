package modules

import com.google.inject.AbstractModule
import service.{RestaurantServiceImpl, RestaurantService}

/**
  * Created by hengkysucanda on 12/5/15.
  */
class ServiceModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[RestaurantService]).to(classOf[RestaurantServiceImpl])
  }
}
