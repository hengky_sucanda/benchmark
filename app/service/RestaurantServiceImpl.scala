package service

import com.google.inject.Inject
import model.Restaurant
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future
import slick.driver.MySQLDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by hengkysucanda on 12/4/15.
  */
class RestaurantServiceImpl @Inject() (dbConfigProvider: DatabaseConfigProvider) extends RestaurantService {
  val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  override def createRestaurant(name: String, email: String, address: String, phone_number: Long): Future[Option[Restaurant]] = {
    db.run(
      (restaurantQuery returning restaurantQuery.map(_.id) into ((restaurant, id) => Some(restaurant.copy(id = id))))
        += Restaurant(1, name, address,  email, phone_number)
    ).recover {
      case ex: Exception =>
        ex.printStackTrace()
        None
    }
  }
}
