package service

import model.Restaurant
import slick.lifted.TableQuery
import tables.RestaurantTable

import scala.concurrent.Future

/**
  * Created by hengkysucanda on 12/4/15.
  */
trait RestaurantService {
  val restaurantQuery = TableQuery[RestaurantTable]
  def createRestaurant(name: String, email: String, address: String, phone_number: Long): Future[Option[Restaurant]]
}
