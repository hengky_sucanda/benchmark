package request

import play.api.libs.json.Json


/**
  * Created by hengkysucanda on 12/4/15.
  */
object RequestFormatter {
  implicit val restaurantFormatter = Json.format[Restaurant]
}
