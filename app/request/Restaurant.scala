package request

/**
  * Created by hengkysucanda on 12/4/15.
  */
case class Restaurant(id: Option[Long] = None, name: String, address: String, email: String, phone_number: Long) {
  def toModel: model.Restaurant = {
    model.Restaurant(1, name, address, email, phone_number)
  }
}