package controllers

import com.google.inject.Inject
import play.api.libs.json.{JsError, Json}
import play.api.mvc._
import request.RequestFormatter._
import request.Restaurant
import service.RestaurantService

import scala.concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global

class Application @Inject()(restaurantService: RestaurantService) extends Controller {

  def index = Action.async(parse.json) {
    implicit request =>
      request.body.validate[Restaurant].map {
        restaurant =>
          restaurantService.createRestaurant(restaurant.name, restaurant.email, restaurant.address, restaurant.phone_number).map {
            case Some(result) =>
              Ok(Json.toJson(Restaurant(Some(result.id), result.name, result.address, result.email, result.phone_number)))
            case None =>
              InternalServerError(Json.toJson("Failed to insert into database"))
          }
      }.recoverTotal {
        ex => Future.apply(BadRequest(JsError.toJson(ex)))
      }
  }

}
