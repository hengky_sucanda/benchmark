package tables

/**
  * Created by hengkysucanda on 12/4/15.
  */

import model.Restaurant
import slick.driver.MySQLDriver.api._
import slick.lifted.ProvenShape

class RestaurantTable(tag: Tag) extends Table[Restaurant](tag, "restaurant"){

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def address = column[String]("address")
  def email = column[String]("email")
  def phone_number = column[Long]("phone_number")

  override def * : ProvenShape[Restaurant] = (id, name, address, email, phone_number) <> (Restaurant.tupled, Restaurant.unapply)
}
